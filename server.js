const http = require('http');
const express = require('express');
const bodyParser =  require("body-parser");
const db = require("./db");
const Controller = require("./controller")
db.sequelize.sync();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req,res)=>{
    res.status(200)
    .send({message:"Your app is running"})
})

app.get("/home",(req,res)=>{
    res.status(200)
    .send({message:"Get home"})
})

app.post("/user",Controller.createUser)

app.get("/user",Controller.getUser)

app.put("/user",Controller.updateUser)

app.delete("/user",Controller.deleteUser)

const server = http.createServer(app);

server.listen(3000,() => {
    console.log(`Server running at port 3000`);
  });