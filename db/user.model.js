module.exports = (sequelize, Sequelize) => {

    const Users = sequelize.define("users", {

        _id: {

            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement:true

        },

        email: {

            type: Sequelize.STRING,
            allowNull: false

        },

        full_name: {

            type: Sequelize.STRING(200),
            allowNull:false

        },

        address: {

            type:Sequelize.STRING(500)

        },

        active:{

            type: Sequelize.ENUM('0','1'),
            defaultValue: '1'
        }

    },
    {timestamps:false}
    );


    return Users;

};