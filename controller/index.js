const db = require('../db')
const Users = db.users;

exports.createUser = async (req, res) => {
    console.log(req.body)
    let user = await Users.create({
        email: req.body.email,
        full_name: req.body.full_name,
        address: req.body.address
    })
    res.send(user)
}

exports.getUser = async (req, res) => {
    console.log(req.query)
    let whereUser = {}
    if (req.query._id)
        whereUser._id = req.query._id
    if (req.query.email)
        whereUser.email = req.query.email

    let user = await Users.findOne({ where: whereUser })
    res.send(user)
}

exports.updateUser = async (req, res) => {
    console.log(req.query)
    let user = await Users.update({ address: req.query.address }, {
        where: {
            _id: req.query._id,
        }
    })
    res.send(user)
}

exports.deleteUser = async (req, res) => {
    console.log(req.query)
    let user = await Users.destroy({
        where: {
            _id: req.query._id,
        }
    })
    if (user)
        res.send({ message: "User deleted" })
    else
        res.send({ message: "No user found" })
}